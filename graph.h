#include <vector>
#include <string>

namespace Graphs {

class Graph {
public:
    Graph(std::string s);
    Graph(const Graph&);
    Graph();
    ~Graph();

    void push(std::string);
    void push(std::string Item, std::string IN);
    void pop() { anotherItems.pop_back(); }
    void clear();
    void getNames() const;

    Graph& operator[] (std::string);
    Graph& operator= (Graph&);

    bool operator==(const Graph&);
    bool operator==(Graph&);
    bool operator!=(const Graph&);
    bool operator!=(Graph&);
private:
    std::string NameIndex;
    Graph* forOperator;

    std::vector<Graph> anotherItems;
    bool findItem(std::string&);
    bool findAndPushItem(std::string& Item, std::string& IN);
};

class SuspendedGraph:Graph
{
public:
    SuspendedGraph(std::string s, int w);
    SuspendedGraph(const SuspendedGraph&);

    void push(std::string s, int w);
    void push(std::string Item, int w, std::string IN);

private:
    int weight;
};

}
