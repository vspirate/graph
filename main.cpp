#include <iostream>
#include "graph.h"

int main()
{
    using namespace Graphs;
    Graph check = Graph("Vova");
    check.push("Slava");
    check.push("Dasha");
    check.push("Jena");
    check.push("Annya", "Dasha");
    check["Vova"].getNames();
    std::cout << std::endl;
    check.push("Dasha", "Annya");
    std::cout << std::endl;
    return 0;
}
