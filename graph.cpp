#include "graph.h"
#include <iostream>
using namespace Graphs;

void Graph::getNames() const
{
    std::cout << NameIndex;
    if(anotherItems.size() == 0)
        return;
    for (unsigned int i = 0; i < anotherItems.size(); i++) {
        std::cout << "-";
        anotherItems[i].getNames();
    }
}

Graph::Graph()
{
    NameIndex = ' ';
    forOperator = nullptr;
}

Graph::~Graph()
{
    clear();
}

Graph::Graph(std::string Name)
{
    NameIndex = Name;
}

Graph::Graph(const Graph& re)
{
    if (*this == re)
        return;
    NameIndex = re.NameIndex;

}

void Graph::push(std::string Name)
{
    if ( findItem(Name) ) {
        std::cout << "Item " << "\"" << Name << "\"" << " has already been\n";
        return;
    }

    Graph* i = new Graph(Name);
    anotherItems.push_back(*i);
    delete i;
}

// haven`t done
void Graph::push(std::string PushItem, std::string InBranch)
{
    if ( findItem(PushItem) ) {
        std::cout << "Item " << "\"" << PushItem << "\"" << " has already been\n";
        return;
    }

    if ( !findItem(InBranch) ) {
        std::cout << "Item " << "\"" << InBranch << "\" not found";
        return;
    }

    findAndPushItem(PushItem, InBranch);
}

void Graph::clear()
{
    NameIndex = ' ';
    forOperator = nullptr;
    anotherItems.clear();
}

// recursion to check all items in graph
// Note: will check all first items from graph and then anothers
bool Graph::findItem(std::string& Item)
{
    // this variable will recurse function
    bool Found = false;
    forOperator = nullptr;

    if ( this->NameIndex == Item) {
        forOperator = this;
        Found = true;
        return Found;
    }
    for (unsigned long i = 0; i < anotherItems.size(); i++) {

        std::string& checkName = anotherItems[i].NameIndex;

        if(Item == checkName) {
            forOperator = &anotherItems[i];
            Found = true;
            break;
        }

        Found = anotherItems[i].findItem(Item);
        forOperator = anotherItems[i].forOperator;
    }

    return Found;
}
bool Graph::findAndPushItem(std::string& Item, std::string& IN)
{
    bool Found = false;

    for (unsigned long i = 0; i < anotherItems.size(); i++) {
        std::string& checkName = anotherItems[i].NameIndex;

        if(IN == checkName) {
            anotherItems[i].push(Item);
            Found = true;
            break;
        }

        Found = anotherItems[i].findAndPushItem(Item, IN);
    }

    return Found;
}


//bool suspendedGraph::findItem(std::string& Name)
//{
//    for (unsigned long i = 0; i < anotherItmes.size(); i++) {
//        std::string& checkName = anotherItmes[i].NameIndex;
//        if(Item == checkName) {
//            valueForFind = anotherItmes[i].value;
//            return true;
//        }

//        // to recuse function
//        findItem(checkName);
//    }
//    valueForFind = 0;
//    return false;
//}
