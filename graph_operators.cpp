#include "graph.h"
using namespace Graphs;

bool Graph::operator==(Graph& s)
{
    if ( NameIndex != s.NameIndex )
        return false;


    for (unsigned long i = 0; i < anotherItems.size(); i++) {
        if ( anotherItems[i].NameIndex != s.anotherItems[i].NameIndex )
            return false;

    }

    return true;
}

bool Graph::operator==(const Graph& s)
{
    if ( NameIndex != s.NameIndex )
        return false;


    for (unsigned long i = 0; i < anotherItems.size(); i++) {
        if ( anotherItems[i].NameIndex != s.anotherItems[i].NameIndex )
            return false;

    }

    return true;
}

bool Graph::operator!=(Graph& s)
{
    return *this == s ? false : true;
}

bool Graph::operator!=(const Graph& s)
{
    return *this == s ? false : true;
}

Graph& Graph::operator= (Graph& re)
{
    if (re == *this)
        return *this;

    Graph *newIt = new Graph;
    newIt->NameIndex = re.NameIndex;

    return *newIt;
}

Graph& Graph::operator[] (std::string Item)
{
    findItem(Item);
    return *forOperator;
}
